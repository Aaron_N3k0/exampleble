package com.n3k0.bleamplemind;

import java.util.HashMap;
import java.util.UUID;

public class SampleGattAttributes {
    // DESCRIPTOR
    public static final UUID UUID_CLIENT_CHARACTERISTIC_CONFIG = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    // CHARACTERISTICS
    public static final UUID UUID_ALL_LIGHT = UUID.fromString("0000ffb5-0000-1000-8000-00805f9b34fb");
    public static final UUID UUID_GREEN_LIGHT = UUID.fromString("0000FFB2-0000-1000-8000-00805f9b34fb");
    public static final UUID UUID_RED_LIGHT = UUID.fromString("0000FFB1-0000-1000-8000-00805F9B34FB");

    private static HashMap<String, String> attributes = new HashMap();

    static {
        attributes.put("00001800-0000-1000-8000-00805f9b34fb", "one");
        attributes.put("0000ffb0-0000-1000-8000-00805f9b34fb", "four");
        attributes.put(UUID_RED_LIGHT.toString(), "\u7ea2\u706fUUID");
        attributes.put(UUID_GREEN_LIGHT.toString(), "\u7eff\u706fUUID");
        attributes.put(UUID_ALL_LIGHT.toString(), "\u5168\u90e8\u63a7\u5236UUID");
    }

    public static String lookup(String uuid, String defaultName) {
        String name = (String) attributes.get(uuid);
        return name == null ? defaultName : name;
    }
}
