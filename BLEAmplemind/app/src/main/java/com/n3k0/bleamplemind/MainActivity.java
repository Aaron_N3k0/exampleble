package com.n3k0.bleamplemind;

import android.Manifest;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class MainActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getSimpleName();

    private BluetoothGattCharacteristic characteristic;

    private ListView lvDevices;

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;
    private int mConnectionState = STATE_DISCONNECTED;

    public final static String ACTION_GATT_CONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.example.bluetooth.le.EXTRA_DATA";

    private int MY_PERMISSIONS_REQUEST_LOCATION = 101;

    private final int REQUEST_ENABLE_BT = 2;
    private BluetoothAdapter btAdapter;
    private BluetoothLeScanner btLeScanner;

    private Button scannButton;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mHandler = new Handler();

        // ================================
        // ASKING FOR BLUETOOTH PERMISSION
        // ================================
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            CheckForPermissions();
        }

        // View Components
        lvDevices = (ListView) findViewById(R.id.lvDevices);
        scannButton = (Button) findViewById(R.id.scannButton);

        // ================================
        // STARTING BLUETOOTH MANAGER
        // ================================
        BluetoothManager btManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        // Setting Adapter
        btAdapter = btManager.getAdapter();
        // Setting Low Energy Scanner
        btLeScanner = btManager.getAdapter().getBluetoothLeScanner();

        // Enabling Bluetooth if not Enabled
        if (btAdapter != null && !btAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }

        // ====================
        // BUTTON SCAN DEVICES
        // ====================
        scannButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "escaneando..", Toast.LENGTH_SHORT).show();
                // ==============
                // Starting Scan
                // ==============
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    btLeScanner.startScan(scanCallback);
                } else {
                    btAdapter.startLeScan(leScanCallback);
                }
                // Stops scanning after a pre-defined scan period.
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "Cancelando escaneo..", Toast.LENGTH_SHORT).show();
                        // ==============
                        // Stopping Scan
                        // ==============
                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            btLeScanner.stopScan(scanCallback);
                        } else {
                            btAdapter.stopLeScan(leScanCallback);
                        }
                    }
                }, 7000);// 7 seg before stop scan
            }
        });
    }

    /**
     * LOW ENERGY CALLBACK SDK < LOLLIPOP
     */
    private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
            if (device.getName() != null)
                Log.d(TAG, "DEVICE Name : " + device.getName() + " MAC_ADDRESS: " + device);

            // name: band-blue DC:02:55:DC:B6:F5
            // name: Band-black: D7:01:55:DC:AC:12

            if (device.toString().equals("D7:01:55:DC:AC:12")) {
                btLeScanner.stopScan(scanCallback);
                device.connectGatt(MainActivity.this, false, btleGattCallback);
            }
        }
    };


    /**
     * LOW ENERGY CALLBACK SDK > LOLLIPOP
     */
    private ScanCallback scanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            BluetoothDevice device = result.getDevice();

            if (device.getName() != null)
                Log.d(TAG, "DEVICE Name : " + device.getName() + " MAC_ADDRESS: " + device);

            // name: band-blue DC:02:55:DC:B6:F5
            // name: Band-black: D7:01:55:DC:AC:12

            if (device.toString().equals("D7:01:55:DC:AC:12")) {
                btAdapter.stopLeScan(leScanCallback);
                device.connectGatt(MainActivity.this, false, btleGattCallback);
            }
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
            Log.d(TAG, "onBatchScanResults: " + results.toString());
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            Log.d(TAG, "onScanFailed errorCode = " + errorCode);
        }
    };

    /**
     * Enable Characteristic Notifications and Write descriptor into Gatt
     *
     * @return if the transaction was successful
     */
    public boolean setCharacteristicNotification(BluetoothGattCharacteristic characteristic,
                                                 BluetoothGattDescriptor descriptor,
                                                 boolean enable,
                                                 BluetoothGatt gatt) {

        gatt.setCharacteristicNotification(characteristic, enable);
        descriptor.setValue(enable ?
                BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE :
                BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
        //descriptor write operation successfully started?
        return gatt.writeDescriptor(descriptor);
    }

    /**
     * Called when some behavior of the Server BLE device change
     */
    private final BluetoothGattCallback btleGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                mConnectionState = STATE_CONNECTED;
                Log.d(TAG, "=======================================================");
                Log.d(TAG, "----> BLE CONNECTED " + gatt.getDevice().getName());
                Log.d(TAG, "=======================================================");
                // =============================
                // Discovering all the services
                // =============================
                gatt.discoverServices();
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                mConnectionState = STATE_DISCONNECTED;
                Log.d(TAG, "=======================================================");
                Log.d(TAG, "----> BLE DISCONNECTED " + gatt.getDevice().getName());
                Log.d(TAG, "=======================================================");
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);

            if (status == BluetoothGatt.GATT_SUCCESS) {

                Log.d(TAG, "\n");
                Log.d(TAG, "======================================================");
                Log.d(TAG, "DESCOVERING SERVICES, CHARACTERISTICS & DESCRIPTORS");
                Log.d(TAG, "======================================================");
                List<BluetoothGattService> services = gatt.getServices();
                String uuid = null;

                // Loops through available GATT Services.
                for (BluetoothGattService gattService : services) {
                    uuid = gattService.getUuid().toString();

                    Log.d(TAG, "[SERVICE " + "UUID " + uuid + "]");

                    List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();

                    // Loops through available Characteristics.
                    for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                        uuid = gattCharacteristic.getUuid().toString();

                        Log.d(TAG, "      * CHARACTERISTIC " + "UUID " + uuid);

                        for (BluetoothGattDescriptor descriptor : gattCharacteristic.getDescriptors()) {
                            uuid = descriptor.getUuid().toString();
                            Log.d(TAG, "          * GATT_DESCRIPTOR " + "UUID " + uuid);
                        }
                    }
                }

                Log.d(TAG, "\n");

                // =============SERVICE==================
                // 0000ffb0-0000-1000-8000-00805f9b34fb
                // ======================================
                BluetoothGattService service =
                        gatt.getService(UUID.fromString("0000ffb0-0000-1000-8000-00805f9b34fb"));

                // ===========CHARACTERISTIC=============
                // 0000ffb2-0000-1000-8000-00805f9b34fb
                // ======================================
                characteristic =
                        service.getCharacteristic(UUID.fromString("0000ffb2-0000-1000-8000-00805f9b34fb"));

                // =============DESCRIPTOR===============
                // 00002902-0000-1000-8000-00805f9b34fb
                // ======================================
                final BluetoothGattDescriptor descriptor =
                        characteristic.getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));

                Log.d(TAG, "================================");
                Log.d(TAG, "    SETTING NOTIFICATIONS        ");
                Log.d(TAG, "================================");
                setCharacteristicNotification(characteristic, descriptor, true, gatt);

                // Gatt created to be use in above Handlers
                final BluetoothGatt mGatt = gatt;

                // ====================================================
                //    WRITING STRING IN DESCRIPTOR AFTER 3 seconds
                // ====================================================
                Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String str = "SNH100115120300001";
                            byte[] sendbyte = str.getBytes();
                            byte[] data = new byte[20];
                            for (int i = 0; i < data.length; i += STATE_CONNECTING) {
                                if (i < sendbyte.length) {
                                    data[i] = sendbyte[i];
                                } else {
                                    data[i] = (byte) 0;
                                }
                            }
                            Log.d(TAG, "======================================================");
                            Log.d(TAG, "              ** WRITING DESCRIPTOR **                ");
                            Log.d(TAG, " SENDING STRING: " + str);
                            Log.d(TAG, " SET VALUE: "+ Arrays.toString(data));
                            Log.d(TAG, "======================================================");
                            descriptor.setValue(data);
                            mGatt.writeDescriptor(descriptor);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 3000);

                // ====================================================
                //    WRITING STRING IN CHARACTERISTIC AFTER 5 seconds
                // ====================================================
                Handler handler4 = new Handler(Looper.getMainLooper());
                handler4.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String str = "BATTERYb";
                            byte[] sendbyte = str.getBytes();
                            byte[] data = new byte[20];
                            for (int i = 0; i < data.length; i += STATE_CONNECTING) {
                                if (i < sendbyte.length) {
                                    data[i] = sendbyte[i];
                                } else {
                                    data[i] = (byte) 0;
                                }
                            }
                            Log.d(TAG, "======================================================");
                            Log.d(TAG, "            ** WRITING DESCRIPTOR **              ");
                            Log.d(TAG, " SENDING STRING: " + str);
                            Log.d(TAG, " SET VALUE: "+ Arrays.toString(data));
                            Log.d(TAG, "======================================================");
                            characteristic.setValue(data);
                            mGatt.writeCharacteristic(characteristic);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 7000);

            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {

                Log.d(TAG, "----> onCharacteristicRead(): " + characteristic.getUuid());

                // For all other profiles, writes the data formatted in HEX.
                final byte[] data = characteristic.getValue();
                if (data != null && data.length > 0) {
                    final StringBuilder stringBuilder = new StringBuilder(data.length);
                    for (byte byteChar : data) {
                        stringBuilder.append(String.format("%02X ", byteChar));
                    }
                    Log.d(TAG, "onCharacteristicRead() DATA READ --> " + "\n" + new String(data) + "\n" + stringBuilder.toString());
                }
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);

            Log.d(TAG, "----> onCharacteristicWrite(): " + characteristic.getUuid());

            try {
                gatt.readCharacteristic(characteristic);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);

            Log.d(TAG, "----> onCharacteristicChanged(): " + characteristic.getUuid());

            final byte[] data = characteristic.getValue();
            if (data != null && data.length > 0) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);
                for (byte byteChar : data) {
                    stringBuilder.append(String.format("%02X ", byteChar));
                }
                Log.d(TAG, "onCharacteristicChanged() DATA READ --> "
                        + "\n" + new String(data) + "\n" + stringBuilder.toString());
            }
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);

            Log.d(TAG, "----> onDescriptorRead(): " + descriptor.getUuid());

            final byte[] data = descriptor.getValue();
            if (data != null && data.length > 0) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);
                for (byte byteChar : data) {
                    stringBuilder.append(String.format("%02X ", byteChar));
                }
                Log.d(TAG, "onDescriptorRead() DATA READ --> "
                        + "\n" + new String(data) + "\n" + stringBuilder.toString());
            }
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);

            Log.d(TAG, "----> onDescriptorWrite(): " + Arrays.toString(descriptor.getValue()));

            try {
                gatt.readDescriptor(descriptor);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
            super.onReliableWriteCompleted(gatt, status);

            Log.d(TAG, "----> onReliableWriteCompleted(): ");
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            super.onReadRemoteRssi(gatt, rssi, status);

            Log.d(TAG, "----> onReadRemoteRssi():");
        }

        @Override
        public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
            super.onMtuChanged(gatt, mtu, status);

            Log.d(TAG, "----> onMtuChanged():");
        }
    };


    private void CheckForPermissions() {
        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION
                        },
                        MY_PERMISSIONS_REQUEST_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
            return;
        }
    }
}
